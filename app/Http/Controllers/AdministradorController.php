<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdministradorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $cantidadCliente = DB::select('SELECT count(*) AS cantidad FROM users WHERE tipo != 0;');
        $cantidadProductosU = DB::select('SELECT count(*) AS cantidad FROM ventas;');
        $cantidadProductosT = DB::select('SELECT SUM(cantidad) AS cantidad FROM ventas;');
        $totalVentas = DB::select('SELECT SUM(total) AS total FROM ventas;');
        return view('administrador.index', compact('cantidadCliente','cantidadProductosU', 'cantidadProductosT','totalVentas'));
    }
}
