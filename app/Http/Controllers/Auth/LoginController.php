<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Auth;
use App\User;

class LoginController extends Controller
{
    use AuthenticatesUsers;


    public function login(Request $request){

        $email = $request->input("email");
        $password = $request->input("password");
        $credentials = $request->only('email','password');
        $usuario = DB::table('users')->where('email', $email)->first();
         if(($usuario->email == $email) && (Hash::check($password, $usuario->password))){
             if($usuario->tipo == 0){
                if(Auth::attempt(['email' => $email, 'password' => $password])){
                    return redirect('administrador');
                }
              }
               elseif($usuario->tipo == 1){
                   if(Auth::attempt(['email' => $email, 'password' => $password])){
                        return redirect('cliente');
                   }
              }
         }else{
             return back()->withErrors(['password' => "El Usuario no existe o la Informacion es Incorrecta"]);
         }

    }

}
