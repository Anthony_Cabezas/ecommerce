<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');



Route::resource('carrito', 'CarritoController')->middleware('auth');
Route::resource('categorias', 'CategoriasController')->middleware('auth');
Route::resource('producto', 'ProductoController')->middleware('auth');
Route::resource('administrador', 'AdministradorController')->middleware('auth');
Route::resource('cliente', 'ClienteController')->middleware('auth');



Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
