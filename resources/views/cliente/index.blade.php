@extends('layouts.app')

@section('content')
    <div class="container">
        <br>
        <div class="row">
            @foreach ($productos as $producto)

            <div class="col-3">
                <div class="card">
                    <img title="{{ $producto->nombre }}"
                        class="card-img-top" src="{{ asset('storage').'/'.$producto->imagen }}"
                        data-toggle="popover" data-trigger="hover"
                        data-content=" {{ $producto->descripcion }}" height="317px">
                    <div class="card-body">
                        <span>{{ $producto->nombre }}</span>
                        <h5 class="card-title">${{ $producto->precio }}</h5>
                        <p class="card-text">{{ $producto->descripcion }}</p>

                        <!-- Este formulario se va a usar para enviar los datos al carrrito de compras -->
                        <form action="{{ url('/cliente') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="producto" id="id"
                                value="{{ $producto->id }}">
                            <input type="hidden" name="nombre" id="nombre"
                                value="{{ $producto->nombre }}">
                            <input type="hidden" name="precio" id="precio"
                                value="{{ $producto->precio }}">
                            <input type="hidden" name="cantidad" id="cantidad"
                                value="1">

                            <button class="btn btn-primary" type="submit" name="" value="agregar">Agregar al
                                Carrito</button>
                        </form>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <script>
        $(function() {
            $('[data-toggle="popover"]').popover()
        })
        </script>
    </div>
</div>
    <!-- Inicio modal estadistica -->
    <div class="modal" id="estadistica" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title text-center" id="exampleModalLabel">Total dinero en compras</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <br>
                        <br>
                    <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
@endsection
