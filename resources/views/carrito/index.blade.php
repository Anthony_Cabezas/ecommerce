
@extends('layouts.app')

@section('content')
    <div class="container">
        <br>
        <h3>Lista del Carrito</h3>
        <table class="table table-dark table-bordered text-center">
            <tbody>
                <tr>
                    <th with="40%">Descripcion</th>
                    <th with="40%">Cantidad</th>
                    <th with="40%">Precio</th>
                    <th with="40%">Total</th>
                    <th with="5%">--</th>
                </tr>
                @php
                    $total=0
                @endphp

                @foreach ($datos as $item)
                <tr>
                    <td with="40%">{{ $item->nombre }}</td>
                    <td with="40%">{{ $item->cantidad }}</td>
                    <td with="40%">{{ $item->precio }}</td>
                    <td with="40%">{{ $item->cantidad * $item->precio }}</td>

                    <td with="5%">
                        <form method="POST" action="{{ url('/carrito' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Categoria" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar</button>
                        </form>
                    </td>
                </tr>
                {{ $total +=  $item->cantidad * $item->precio}}
                @endforeach
                <tr>
                    <td colspan="3" align="right">
                        <h3>Total</h3>
                    </td>
                    <td align="center">
                        <h3>${{ $total }}</h3>
                    </td>
                    <td><form action="" method="post">
                            <button class="btn btn-success" type="submit" name="btnPagar"
                                value="pagar">Pagar</button>
                        </form></td>
                </tr>
            </tbody>
        </table>



    </div>
@endsection
