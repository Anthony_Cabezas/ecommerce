@extends('layouts.app')

@section('content')
        <br>
        <div class="container-xl">
            <!-- Editable table -->
            <div class="card">
                <h3 class="card-header text-center font-weight-bold text-uppercase py-4">Estadisticas</h3>
                <div class="card-body">
                    <div id="table" class="table-editable">
                        <table class="table table-bordered table-responsive-md table-striped text-center table-dark"
                            id="example">
                            <thead>
                                <tr>
                                    <th class="text-center">Cantidad de Clientes Registrados</th>
                                    <th class="text-center">Cantidad de productos unicos vendidos</th>
                                    <th class="text-center">Cantidad de productos totales vendidos</th>
                                    <th class="text-center">Monto total de ventas</th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr>
                                    @foreach ($cantidadCliente as $item)
                                        <td class="pt-3-half">{{ $item->cantidad }}</td>
                                    @endforeach

                                    @foreach ($cantidadProductosU as $item)
                                        <td class="pt-3-half">{{ $item->cantidad }}</td>
                                    @endforeach

                                    @foreach ($cantidadProductosT as $item)
                                        @if ($item->cantidad == null)
                                            <td class="pt-3-half">{{ 0 }}</td>
                                        @else
                                            <td class="pt-3-half">{{ $item->cantidad }}</td>
                                        @endif
                                    @endforeach

                                    @foreach ($totalVentas as $item)
                                        @if ($item->total == null)
                                            <td class="pt-3-half">{{ 0 }}</td>
                                        @else
                                            <td class="pt-3-half">{{ $item->total }}</td>
                                        @endif
                                    @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

