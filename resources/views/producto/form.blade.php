<div class="form-group">
    <label for="SKU" class="control-label">{{ 'Sku' }}</label>
    <input class="form-control {{ $errors->has('SKU') ? 'is-invalid' : ''}}" name="SKU" type="text" id="SKU"
    value="{{ isset($producto->SKU) ? $producto->SKU : old('SKU')}}" >
    {!! $errors->first('SKU', '<p class="invalid-feedback">:message</p>') !!}
</div>
<div class="form-group ">
    <label for="nombre" class="control-label">{{ 'Nombre' }}</label>
    <input class="form-control {{ $errors->has('nombre') ? 'is-invalid' : ''}}" name="nombre" type="text" id="nombre"
    value="{{ isset($producto->nombre) ? $producto->nombre : old('nombre')}}" >
    {!! $errors->first('nombre', '<p class="invalid-feedback">:message</p>') !!}
</div>
<div class="form-group">
    <label for="descripcion" class="control-label">{{ 'Descripcion' }}</label>
    <input class="form-control {{ $errors->has('descripcion') ? 'is-invalid' : ''}}" name="descripcion" type="text" id="descripcion"
    value="{{ isset($producto->descripcion) ? $producto->descripcion : old('descripcion')}}" >
    {!! $errors->first('descripcion', '<p class="invalid-feedback">:message</p>') !!}
</div>
<div class="form-group">
    <label for="imagen" class="control-label">{{ 'Imagen' }}</label>
    <input class="form-control {{ $errors->has('imagen') ? 'is-invalid' : ''}}" name="imagen" type="file" id="imagen"
    value="{{ isset($producto->imagen) ? $producto->imagen : ''}}" >
    {!! $errors->first('imagen', '<p class="invalid-feedback">:message</p>') !!}
</div>
<div class="form-group">
    <label for="categoria" class="control-label">{{ 'Categoria' }}</label>
    <select name="categoria" class="form-control {{ $errors->has('categoria') ? 'is-invalid' : ''}}" id="categoria">
    <option value="" disabled selected>Seleccione una categoria</option>
    @foreach ($datos as $dato)
        <option value="{{ $dato->id }}">{{ $dato->nombre }}</option>
    @endforeach
    {{-- @foreach (json_decode('{}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($producto->categoria) && $producto->categoria == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach --}}
</select>
    {!! $errors->first('categoria', '<p class="invalid-feedback">:message</p>') !!}
</div>
<div class="form-group">
    <label for="stock" class="control-label">{{ 'Stock' }}</label>
    <input class="form-control {{ $errors->has('stock') ? 'is-invalid' : ''}}" name="stock" type="number" id="stock"
    value="{{ isset($producto->stock) ? $producto->stock : ''}}" >
    {!! $errors->first('stock', '<p class="invalid-feedback">:message</p>') !!}
</div>
<div class="form-group">
    <label for="precio" class="control-label">{{ 'Precio' }}</label>
    <input class="form-control {{ $errors->has('precio') ? 'is-invalid' : ''}}" name="precio" type="number" id="precio" value="{{ isset($producto->precio) ? $producto->precio : ''}}" >
    {!! $errors->first('precio', '<p class="invalid-feedback">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Actulizar' : 'Crear' }}">
</div>
