<div class="form-group ">
    <label for="nombre" class="control-label">{{ 'Nombre' }}</label>
    <input class="form-control {{ $errors->has('nombre') ? 'is-invalid' : ''}}" name="nombre" type="text" id="nombre" value="{{ isset($categoria->nombre) ? $categoria->nombre : ''}}" >
    {!! $errors->first('nombre', '<p class="invalid-feedback">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Actualizar' : 'Crear' }}">
</div>
